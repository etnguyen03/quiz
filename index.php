<!-- Ethan Voting
Version 1.1

Copyright 2015 Ethan Nguyen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Voting</title>
    <link rel="stylesheet" href="css/main.css" media="screen" title="no title" charset="utf-8">
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.4.js" charset="utf-8"></script>
    <script src="js/mobile.js" charset="utf-8"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.4/jquery-ui.js" charset="utf-8"></script>
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.4/themes/smoothness/jquery-ui.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile.structure-1.4.5.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile.theme-1.4.5.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile-1.4.5.css" media="screen" title="no title" charset="utf-8">
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.mobile/1.4.5/jquery.mobile-1.4.5.js" charset="utf-8"></script>
  </head>
  <body>
    <div class="header" data-role="header">
      <h1>Ethan Quiz</h1>
    </div>
    <h2>Enter Question ID</h2>
    <form id='pollidform'>
      <input type="number" id="pollid" placeholder='Poll ID' required>
      <input type="submit" id='pollbutton' value="Submit"  data-icon="check">
    </form>
    <div id='jsblurb'>
      <strong><font style='color: red;'>Please enable JavaScript, then reload the page</font></strong>
    </div>
    <p id='instructions'>

    </p>
    <div class="choices">
      <form id="pollform">
        <fieldset data-role="controlgroup">
          <legend>Select your option: </legend>
          <div class="items">

          </div>
        </fieldset>
        <br>
        <input type="submit" id='polllbutton' value="Vote" data-icon="check">
        <button type="button" id='back' data-icon="back">Back</button>
      </form>
    </div>
    <div class="hidden">
      <div id="error" title='Error'>
        <p>

        </p>
      </div>
      <div id='conf' title='Are you sure?'>
        <p>

        </p>
      </div>
    </div>
    <div class="footer" data-role="footer">
      <h4>Copyright (c) 2015 Ethan Nguyen. All Rights Reserved. <a href="license.php">License</a></h4>
    </div>
  </body>
</html>
