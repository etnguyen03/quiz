**Doesn't work just yet!**

# Quiz
Quiz software for PHP

## How to implement
Download all files and place them where desired.

### Connect Files
In `/admin/ajax/connect.php` and `/ajax/connect.php`, replace the connect line with your own preference.
Example:
```php
<?php
  // Ethan Quiz
  // Version 1.1
  //
  // Copyright 2015 Ethan Nguyen
  //
  // Licensed under the Apache License, Version 2.0 (the "License");
  // you may not use this file except in compliance with the License.
  // You may obtain a copy of the License at
  //
  //  http://www.apache.org/licenses/LICENSE-2.0
  //
  // Unless required by applicable law or agreed to in writing, software
  // distributed under the License is distributed on an "AS IS" BASIS,
  // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  // See the License for the specific language governing permissions and
  // limitations under the License.

  $db = new mysqli("localhost", "root", "mypassword", "voting");
 ?>
```

### MySQL Database

Create a database, if not already done.
Then, create these two tables.

#### Questions

Create a table named "questions", case sensitive, without quotes.
Fields:

Name | Type | Primary | Extra
--- | --- | --- | ---
id | bigint | yes | auto_increment
choices | varchar(1000) | no | no
instructions | varchar(1000) | no | no
title | varchar(250) | no | no
disabled | tinyint(1) | no | no

#### Answers

Create a table named "answers", case sensitive, without quotes.
Fields:

Name | Type | Primary | Extra
--- | --- | --- | ---
id | bigint | yes | auto_increment
cookie | varchar(250) | no | no
choice | varchar(250) | no | no
pollid | bigint | no | no

### htpasswd & htaccess

In `/admin/.htaccess`, there is a sample .htaccess. Change to suit needs, then create a .htpasswd file corresponding to the options in the .htaccess file.