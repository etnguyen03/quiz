// Ethan Quiz
// Version 1.1
//
// Copyright 2015 Ethan Nguyen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

$(document).ready(function(){
  $(".jsblurb").hide();
  $(".pollidform").show();
  $("#create").click(function () {
    $.post("ajax/server.php", {action: "9"}, function (data) {
      console.log(data);
      $("#errorD p").html("Your Poll ID is: " + data + ". You will be entered into the administrator panel, please note that your poll starts off being disabled.");
      $("#errorD").dialog({
        modal: true,
        close: function () {
          $(this).dialog("close");
          $("#pollid").val(data);
          $("#pollidb").click();
        }
      });
    });
  });
  $("#pollidb").click(function(e){
    var pollid = $("#pollid").val();
    $.post("ajax/server.php", {action: "1", pollid: pollid}, function(data){
      var parsed = $.parseJSON(data);
      if (typeof parsed.errorcode == "undefined") {
        parsed.choices = $.parseJSON(parsed.choices);
        $(".pollidform").hide();
        $(".administr").show();

        updatevotes(parsed.id);

        $("#refresh").click(function(){
          updatevotes(parsed.id);
        });

        $(".pollidsetup").html(parsed.id);
        $(".polltitle").html(parsed.title);
        $("#namebox").val(parsed.title);

        if (parsed.disabled == 1) {
          $("#disable").html("disabled");
        }
        else {
          $("#disable").html("not disabled");
        }

        $("#disabler").click(function(){
          $.post("ajax/server.php", {action: "8", pollid: parsed.id}, function (data) {
            if (data == "1") {
              if ($("#disable").html() == "disabled") {
                $("#disable").html("not disabled");
              }
              else {
                $("#disable").html("disabled");
              }
            }
          })
        });

        $("#descriptionbox").val(parsed.instructions);

        $.each(parsed.choices, function(key, value){
          $("#optionslist").append("<li><input type='text' title='Autosaves' class='tooltip options' id='opt"+ key +"' value='"+value+"' placeholder='Option "+ key +"'></li>");
        });
        $(".tooltip").tooltip();

        $("#optionslist").append("<button id='addmore'>Add Another Option</button>");
        $("#optionslist").append("<button class='delete'>Delete Last Choice</button>")

        $("#addmore").click(function(){
          var pollid = $(".pollidsetup").html();
          $.post("ajax/server.php", {action: "5", pollid: pollid}, function (data) {
            data = parseInt(data) + 1;
            $("#addmore").before("<li><input type='text' title='Autosaves' class='tooltip options' id='opt"+ data +"' placeholder='Option "+ data +"'></li>");
            $(".tooltip").tooltip();
            $(".options").change(function(){
              var optid = $(this).attr("id");
              optid = optid.replace("opt", "");
              optid = parseInt(optid);

              var value = $(this).val();
              var pollid = $(".pollidsetup").html();

              $.post("ajax/server.php", {action: "4", optid: optid, value: value, pollid: pollid});
            });
          });
        });

        $(".options").change(function(){
          var optid = $(this).attr("id");
          optid = optid.replace("opt", "");
          optid = parseInt(optid);

          var value = $(this).val();
          var pollid = $(".pollidsetup").html();

          $.post("ajax/server.php", {action: "4", optid: optid, value: value, pollid: pollid});
        });

        $(".delete").click(function(){
          var pollid = $(".pollidsetup").html();

          $.post("ajax/server.php", {action: "6", pollid: pollid}, function (data) {
            $("li").filter(":last").remove();
          });
        });

        $(".administr").accordion({collapsible: true});
      }
      else {
        $("#errorD p").html(parsed.description);
        $("#errorD").dialog({
          modal: true
        });
      }
    });
    e.preventDefault();
  });
  $("#namebox").change(function(){
    var name = $("#namebox").val();
    var pollid = $(".pollidsetup").html();
    $.post("ajax/server.php", {action: "2", pollid: pollid, name: name}, function(data){
      if (data == 1) {
        $(".polltitle").html(name);
      }
    });
  });
  $("#descriptionbox").change(function(){
    var description = $("#descriptionbox").val();
    var pollid = $(".pollidsetup").html();
    $.post("ajax/server.php", {action: "3", pollid: pollid, description:description});
  });
});

function updatevotes(pollid) {
  $.post("ajax/server.php", {action: "7", pollid: pollid}, function(data){
    var parsedv = $.parseJSON(data);
    var votesdisp;
    $.each(parsedv, function (key, value) {
      votesdisp = votesdisp + "<tr class='ting'><td>" + key + "</td><td>"+value.val+"</td><td>"+value.votes+"</td></tr>";
    });
    if ($(".ting").length > 0) {
      $(".ting").remove();
    }
    $("#votetable tr").first().after(votesdisp);
  });
}
