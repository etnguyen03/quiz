<!-- Ethan Quiz
Version 1.1

Copyright 2015 Ethan Nguyen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Voting - Administrator</title>
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.4.js" charset="utf-8"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.4/jquery-ui.js" charset="utf-8"></script>
    <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.4/themes/smoothness/jquery-ui.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="css/main.css" media="screen" title="no title" charset="utf-8">
    <script src="js/main.js" charset="utf-8"></script>
  </head>
  <body>
    <h1>Ethan Voting - Administrator</h1>
    <div class="jsblurb">
      <strong><font style='color: red;'>Please enable JavaScript, then reload the page</font></strong>
    </div>
    <div class="pollidform">
      <form class="pollidform">
        <input type="text" id='pollid' placeholder="Poll ID" required>
        <input type="submit" id='pollidb' value="Submit">
      </form>
      <div style='padding-top: 10px;'>
        <button type="button" id='create'>Create</button>
      </div>
    </div>
    <h2 class='polltitle'></h2>
    <div class="administr">
      <h3>Poll ID (setup for voters)</h3>
      <div>
        <strong align="center" class='ui-widget-header header'>Please go to this website:</strong><br>
        <span class='header-bigger'><input type='text' placeholder='Path to access client voting' style='font-size: 4em; width: 90%; outline: none; '></span><br>
        <strong align="center" class='ui-widget-header header'>The Poll ID is:</strong><br>
        <span class='pollidsetup'></span>
      </div>
      <h3>Name</h3>
      <div>
        <input type="text" id='namebox' value="" placeholder="Name" title='Autosaves' class='tooltip' maxlength="250">
      </div>
      <h3>Instructions</h3>
      <div>
        <!-- <input type="text" id='descriptionbox' value="" placeholder="Description" title='Autosaves' class='tooltip' maxlength="1000"> -->
        <textarea id='descriptionbox' rows="8" cols="40" placeholder="Instructions" title="Autosaves" class="tooltip" maxlength="1000"></textarea>
      </div>
      <h3>Options</h3>
      <div>
        <ul id='optionslist'>

        </ul>
      </div>
      <h3>Votes</h3>
      <div>
        <table id='votetable' class='ui-widget'>
          <tr class='ui-widget-header'>
            <th>
              Option Number
            </th>
            <th>
              Option Text
            </th>
            <th>
              Votes
            </th>
          </tr>
        </table>
        <div style='padding-top: 10px;'>
          <button type="button" id='refresh'>Refresh Stats</button>
        </div>
      </div>
      <h3>Disabling</h3>
      <div>
        This poll is <strong id='disable'></strong>.
        <button type="button" id="disabler">Toggle</button>
      </div>
    </div>
    <div class="hidden">
      <div id='errorD' title='Error'>
        <p>

        </p>
      </div>
    </div>
    <div class="footer">
      Copyright (c) 2015 Ethan Nguyen. All Rights Reserved.
    </div>
  </body>
</html>
