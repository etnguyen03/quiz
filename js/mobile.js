// Ethan Quiz
// Version 1.1
//
// Copyright 2015 Ethan Nguyen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

$(document).bind('pageinit', function(){
  $("#pollidform").show();
  $("#jsblurb").hide();
  $("#pollbutton").button('disable');
  $("#pollbutton").button('refresh');
  $("#pollid").bind("change", function () {
    if ($("#pollid").val() != "") {
      $("#pollbutton").button('enable');
      $("#pollbutton").button('refresh');
    }
    else {
      $("#pollbutton").button('disable');
      $("#pollbutton").button('refresh');
    }
  })
  $("#pollbutton").bind('tap', function(e){
    $("#pollbutton").button('disable');
    $("#pollbutton").button('refresh');
    var pollid = $("#pollid").val();
    if (pollid != "") {
      initui(pollid);
      e.preventDefault();
    }
  });
  $("#polllbutton").bind('tap', function(e){
    var choice = $("input[name^=vote]:checked").attr("id");
    var choice = choice.replace("vote", "");
    var choice = parseInt(choice);

    var choiceval = $("input[name^=vote]:checked").val();

    var pollid = $("input[name^=vote]:checked").attr("name");
    var pollid = pollid.replace("vote", "");
    var pollid = parseInt(pollid);


    registervote(pollid, choice);


    e.preventDefault();
  });
});

function initui(pollid) {
  $.post("ajax/server.php", {action: "1", pollid: pollid}, function(data){
    console.log(data);
    var array = $.parseJSON(data);
    if (typeof array.errorcode == "undefined") {
      $("h2").text(array.title);
      $("#pollidform").css("display", "none");
      $("#pollform").css("display", "inline");
      $("#instructions").html(array.instructions);
      $("#choices").css("display", "inline");
      array.choices = $.parseJSON(array.choices);
      var items = "";
      $.each(array.choices, function(key, value){
        items = items + "<input type='radio' name='vote"+ pollid +"'  id='vote"+ key +"' value='"+ value +"'> <label for='vote"+key+"'>" + value + "</label>";
      });
      $(".items").html(items);
      $("input[type=radio]").checkboxradio();
      $("#back").bind('tap', function () {
        $("#pollidform").css("display", "inline");
        $("#pollform").css("display", "none");
        $("#pollid").val("");
        $("h2").text("Enter Poll ID");
        $("#instructions").html("");
        $("input").removeAttr("disabled");
        $("#polllbutton").val("Vote");
        $("#polllbutton").button('refresh');
      });
    }
    else if (array.errorcode == "1") {
      $("#pollbutton").button('enable');
      $("#pollbutton").button('refresh');
      alert(array.description + ", please try again.");
    }
    else {
      $("#pollbutton").button('enable');
      $("#pollbutton").button('refresh');
      alert(array.description);
    }
  });
}

function registervote(pollid, votechoice) {
  $.post("ajax/server.php", {action: "2", pollid: pollid, votechoice: votechoice}, function (data) {
    if (data == "1") {
      $("input").attr("disabled", "disabled");
      $("#polllbutton").val("Your vote has been casted");
      $("#polllbutton").button('refresh');
      $("input[type=radio]").checkboxradio('refresh');
    }
    else {
      console.log(data);
    }
  })
}
