<?php

// Ethan Quiz
// Version 1.1
//
// Copyright 2015 Ethan Nguyen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

  $action = $_POST['action'];
  require 'connect.php';
  if ($action == "1") {
    $pollid = $_POST['pollid'];
    $query = $db->query("SELECT * FROM polls WHERE disabled=0 AND id='$pollid'");
    if ($query->num_rows == "1") {
      $sending = json_encode(mysqli_fetch_assoc($query));
      if (isset($_COOKIE['ETHANVOTINGCOOKIE'])) {
        $cookie = $_COOKIE['ETHANVOTINGCOOKIE'];
        $cookie = json_decode($cookie, true);
        if (isset($cookie[$pollid])) {
          $query = $db->query("SELECT * FROM votes WHERE cookie='".$cookie[$pollid]."'");
          if ($query->num_rows == 1) {
            echo json_encode(array('errorcode' => 2, 'description' => 'This computer has already voted.'));
          }
          else {
            echo $sending;
          }
        }
        else {
          echo $sending;
        }
      }
      else {
        echo $sending;
      }
    }
    else {
      echo json_encode(array('errorcode' => 1, 'description' => 'Invalid Poll ID'));
    }
  }
  elseif ($action == "2") {
    $pollid = $_POST['pollid'];
    $votechoice = $_POST['votechoice'];

    $a = false;
    $cookiev = "";
    while ($a == false) {
      $cookiev = base64_encode(openssl_random_pseudo_bytes(100));
      $query = $db->query("SELECT * FROM votes WHERE cookie='$cookiev'");
      if ($query->num_rows == 0) {
        $a = true;
      }
    }

    $db->query("INSERT INTO votes (cookie, choice, pollid) VALUES ('$cookiev', '$votechoice', '$pollid')");

    if (isset($_COOKIE['ETHANVOTINGCOOKIE'])) {
      $cookie = $_COOKIE['ETHANVOTINGCOOKIE'];
      $cookie = json_decode($cookie, true);
    }
    $cookie[$pollid] = $cookiev;
    $cookie = json_encode($cookie);

    setcookie("ETHANVOTINGCOOKIE", $cookie, time()+60*60*24*30000);

    echo "1";
  }
 ?>
